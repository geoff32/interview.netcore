﻿var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCandidates();
builder.Services.AddControllersWithViews();
builder.Services.AddWebApiSwagger();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseWebApiSwagger();

app.MapControllers();
app.Run();
