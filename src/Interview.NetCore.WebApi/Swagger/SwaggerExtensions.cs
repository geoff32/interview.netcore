using Microsoft.OpenApi.Models;

namespace Microsoft.Extensions.DependencyInjection;

public static class SwaggerExtensions
{
    public static void AddWebApiSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(options => options.SwaggerDoc("webapi", new OpenApiInfo()));
    }

    public static void UseWebApiSwagger(this IApplicationBuilder app)
    {
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.RoutePrefix = string.Empty;
            options.SwaggerEndpoint("/swagger/webapi/swagger.json", "WebApi");
        });
    }
}