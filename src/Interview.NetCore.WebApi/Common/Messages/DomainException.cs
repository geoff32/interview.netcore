namespace Interview.NetCore.WebApi.Common.Messages;

[Serializable]
public class DomainException : ManagedException
{
    public DomainException() { }
    public DomainException(string message) : base(message) { }
    public DomainException(string message, Exception inner) : base(message, inner) { }
}