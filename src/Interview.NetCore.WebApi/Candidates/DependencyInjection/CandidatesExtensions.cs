using Interview.NetCore.WebApi.Candidates.Application;
using Interview.NetCore.WebApi.Candidates.Application.Abstractions;
using Interview.NetCore.WebApi.Candidates.Application.Abstractions.Mappers;
using Interview.NetCore.WebApi.Candidates.Application.Mappers;
using Interview.NetCore.WebApi.Candidates.Domain.Infrastructure;
using Interview.NetCore.WebApi.Candidates.Repositories;

namespace Microsoft.Extensions.DependencyInjection;

public static class CandidatesExtensions
{
    public static void AddCandidates(this IServiceCollection services)
    {
        services.AddScoped<ICandidateService, CandidateService>();
        services.AddSingleton<ICandidateMapper, CandidateMapper>();
        services.AddScoped<ICandidateRepository, CandidateRepository>();
    }
}