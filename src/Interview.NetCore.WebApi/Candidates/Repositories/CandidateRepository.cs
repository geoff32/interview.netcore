using Interview.NetCore.WebApi.Candidates.Domain;
using Interview.NetCore.WebApi.Candidates.Domain.Abstractions;
using Interview.NetCore.WebApi.Candidates.Domain.Infrastructure;

namespace Interview.NetCore.WebApi.Candidates.Repositories;

public class CandidateRepository : ICandidateRepository
{
    private readonly IDictionary<Guid, ICandidate> _candidates;
    private readonly object _lock = new object();

    public CandidateRepository()
    {
        _candidates = Enumerable.Range(1, 5)
            .Select(i => new Candidate(Guid.NewGuid(), $"FirstName {i}", $"LastName {i}"))
            .Cast<ICandidate>()
            .ToDictionary(candidate => candidate.Id);
    }

    public ICandidate? Get(Guid id)
        => _candidates.TryGetValue(id, out var candidate) ? candidate : null;

    public IEnumerable<ICandidate> GetAll() => [.. _candidates.Values];


    public bool Remove(Guid id)
    {
        lock (_lock)
        {
            return _candidates.Remove(id);
        }
    }

    public bool Set(ICandidate? candidate)
    {
        lock (_lock)
        {
            if (candidate == null)
            {
                return false;
            }

            _candidates[candidate.Id] = candidate;
            return true;
        }
    }
}