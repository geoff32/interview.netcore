using Interview.NetCore.WebApi.Candidates.Application.Abstractions;
using Interview.NetCore.WebApi.Candidates.Application.Models;
using Interview.NetCore.WebApi.Common.Messages;
using Microsoft.AspNetCore.Mvc;

namespace Interview.NetCore.WebApi.Candidates;

[Route("api/candidates")]
public class CandidateController : ControllerBase
{
    private readonly ICandidateService _candidateService;

    public CandidateController(ICandidateService candidateService)
        => _candidateService = candidateService;

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Candidate>), 200)]
    public IActionResult GetCandidates()
    {
        try
        {
            return Ok(_candidateService.GetCandidates());
        }
        catch (ManagedException ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(Candidate), 200)]
    [ProducesResponseType(404)]
    public IActionResult GetCandidate(Guid id)
    {
        try
        {
            var candidate = _candidateService.GetCandidate(id);
            return candidate == null ? (IActionResult)NotFound() : Ok(candidate);
        }
        catch (ManagedException ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(204)]
    [ProducesResponseType(400)]
    public IActionResult RemoveCandidate(Guid id)
    {
        try
        {
            return _candidateService.RemoveCandidate(id) ? (IActionResult)NoContent() : BadRequest();
        }
        catch (ManagedException ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(CreateCandidate), 200)]
    [ProducesResponseType(400)]
    public IActionResult CreateCandidate(CreateCandidate candidate)
    {
        try
        {
            var id = _candidateService.CreateCandidate(candidate);
            return id.HasValue ? (IActionResult)Ok(id.Value) : BadRequest();
        }
        catch (ManagedException ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPut]
    [ProducesResponseType(typeof(Candidate), 200)]
    [ProducesResponseType(400)]
    public IActionResult UpdateCandidate(Candidate candidate)
    {
        try
        {
            return _candidateService.UpdateCandidate(candidate) ? (IActionResult)Ok(candidate) : BadRequest();
        }
        catch (ManagedException ex)
        {
            return BadRequest(ex.Message);
        }
    }
}