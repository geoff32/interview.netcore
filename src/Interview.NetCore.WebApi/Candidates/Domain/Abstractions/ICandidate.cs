namespace Interview.NetCore.WebApi.Candidates.Domain.Abstractions;

public interface ICandidate
{
    Guid Id { get; }
    string FirstName { get; }
    string LastName { get; }
}