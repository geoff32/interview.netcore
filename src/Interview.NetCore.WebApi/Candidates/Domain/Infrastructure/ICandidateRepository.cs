using Interview.NetCore.WebApi.Candidates.Domain.Abstractions;

namespace Interview.NetCore.WebApi.Candidates.Domain.Infrastructure;

public interface ICandidateRepository
{
    IEnumerable<ICandidate> GetAll();
    ICandidate? Get(Guid id);
    bool Remove(Guid id);
    bool Set(ICandidate? candidate);
}