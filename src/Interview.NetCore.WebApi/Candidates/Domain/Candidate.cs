using Interview.NetCore.WebApi.Candidates.Domain.Abstractions;
using Interview.NetCore.WebApi.Common.Messages;

namespace Interview.NetCore.WebApi.Candidates.Domain;

public class Candidate : ICandidate
{
    public Candidate(Guid id, string firstName, string lastName)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;

        CheckIntegrity();
    }

    public Guid Id { get; }
    public string FirstName { get; }
    public string LastName { get; }

    private void CheckIntegrity()
    {
        if (string.IsNullOrEmpty(LastName))
        {
            throw new DomainException("LastName is required");
        }
    }
}