using Interview.NetCore.WebApi.Candidates.Application.Models;

namespace Interview.NetCore.WebApi.Candidates.Application.Abstractions;

public interface ICandidateService
{
    IEnumerable<Candidate> GetCandidates();
    Candidate? GetCandidate(Guid id);
    bool RemoveCandidate(Guid id);
    Guid? CreateCandidate(CreateCandidate candidateModel);
    bool UpdateCandidate(Candidate candidateModel);
}