using System.Diagnostics.CodeAnalysis;
using Interview.NetCore.WebApi.Candidates.Application.Models;
using Interview.NetCore.WebApi.Candidates.Domain.Abstractions;

namespace Interview.NetCore.WebApi.Candidates.Application.Abstractions.Mappers;

public interface ICandidateMapper
{
    [return:NotNullIfNotNull(nameof(candidateModel))]
    ICandidate? ToCandidate(CreateCandidate? candidateModel);
    [return:NotNullIfNotNull(nameof(candidateModel))]
    ICandidate? ToCandidate(Candidate? candidateModel);
    [return:NotNullIfNotNull(nameof(candidate))]
    Candidate? ToCandidateModel(ICandidate? candidate);
}