using Interview.NetCore.WebApi.Candidates.Application.Abstractions.Mappers;
using Interview.NetCore.WebApi.Candidates.Application.Models;
using Interview.NetCore.WebApi.Candidates.Domain.Abstractions;

namespace Interview.NetCore.WebApi.Candidates.Application.Mappers;

public class CandidateMapper : ICandidateMapper
{
    public ICandidate? ToCandidate(Candidate? candidateModel)
        => candidateModel == null ? null : new Domain.Candidate(candidateModel.Id, candidateModel.FirstName, candidateModel.LastName);

    public ICandidate? ToCandidate(CreateCandidate? candidateModel)
        => candidateModel == null ? null : new Domain.Candidate(Guid.NewGuid(), candidateModel.FirstName, candidateModel.LastName);

    public Candidate? ToCandidateModel(ICandidate? candidate)
        => candidate == null ? null : new Candidate(candidate.Id, candidate.FirstName, candidate.LastName);
}