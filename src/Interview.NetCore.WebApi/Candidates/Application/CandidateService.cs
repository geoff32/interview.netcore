using Interview.NetCore.WebApi.Candidates.Application.Abstractions;
using Interview.NetCore.WebApi.Candidates.Application.Abstractions.Mappers;
using Interview.NetCore.WebApi.Candidates.Application.Models;
using Interview.NetCore.WebApi.Candidates.Domain.Infrastructure;

namespace Interview.NetCore.WebApi.Candidates.Application;

public class CandidateService : ICandidateService
{
    private readonly ICandidateMapper _candidateMapper;
    private readonly ICandidateRepository _candidateRepository;

    public CandidateService(ICandidateMapper candidateMapper, ICandidateRepository candidateRepository)
    {
        _candidateMapper = candidateMapper;
        _candidateRepository = candidateRepository;
    }


    public Guid? CreateCandidate(CreateCandidate candidateModel)
    {
        var candidate = _candidateMapper.ToCandidate(candidateModel);
        return _candidateRepository.Set(candidate) ? candidate?.Id : null;
    }

    public Candidate? GetCandidate(Guid id)
        => _candidateMapper.ToCandidateModel(_candidateRepository.Get(id));


    public IEnumerable<Candidate> GetCandidates()
        => _candidateRepository.GetAll().Select(candidate => _candidateMapper.ToCandidateModel(candidate));

    public bool RemoveCandidate(Guid id) => _candidateRepository.Remove(id);

    public bool UpdateCandidate(Candidate candidateModel)
        => _candidateRepository.Set(_candidateMapper.ToCandidate(candidateModel));
}