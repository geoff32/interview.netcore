namespace Interview.NetCore.WebApi.Candidates.Application.Models;

public record Candidate(Guid Id, string FirstName, string LastName);
