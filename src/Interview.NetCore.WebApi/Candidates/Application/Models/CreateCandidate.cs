namespace Interview.NetCore.WebApi.Candidates.Application.Models;

public record CreateCandidate(string FirstName, string LastName);
